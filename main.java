import java.io.*; 
import java.util.*; 
import java.util.concurrent.locks.ReentrantLock;

class Semaphore
{
    private int sem = 1;

    // synchronized : évite l'interruption par un autre thread
    public synchronized void prendre() throws InterruptedException
    {
        if(sem == 0)
        {
            // Deadlock solved: Do not wait more than 2s
            wait(2000); // Attente indéfinie d'une notification par le thread qui possède la ressource
        }
        sem = sem - 1;
    }

    public synchronized void liberer() 
    {
        sem = sem + 1;
        notify(); // Réveille le premier thread bloqué sur wait
    }
}

class Ingredient extends Semaphore {
    private String name;

    Ingredient(String name) {
        this.name= name;
    }

    public String Name() {
        return this.name;
    }
}

class Barman extends Thread {

    private String cocktail;
    private String name;
    private ArrayList<Ingredient> ingredients;

    Barman(String name, String cocktail) {
        this.name = name;
        this.cocktail = cocktail;
        this.ingredients = new ArrayList<>();
    }

    public void lockIngredients() {
        for(Ingredient ingredient : this.ingredients) {
            try {
                ingredient.prendre();
                // Finding ingredient takes time...
                // This code stops the execution (deadlock)
                //
                // try {
                //     Thread.sleep(300);
                // } catch (InterruptedException e) {
                //     System.exit(0);
                // }
            } catch (InterruptedException e) {
                System.out.printf("Could not take ingredient %s, Error: %s\n", ingredient.Name(), e);
                System.exit(0);
            }
        }
    }

    public void freeIngredients() {
        for(Ingredient ingredient : this.ingredients) {
            ingredient.liberer();
        }
    }

    public void makeCocktail() {
        System.out.printf("Hello, I %s am preparing the cocktail: %s...\n", this.name, this.cocktail);
        try {
            Thread.sleep(750);
            System.out.printf("Sir, your cocktail %s is ready.\n\n", this.cocktail);
        } catch (InterruptedException e) {
            System.exit(0);
        }
    }

    public void setIngredients(Ingredient... ingredients) {
        for(Ingredient ingredient : ingredients){
            this.ingredients.add(ingredient);
        }
    }


    @Override
    public void run() {
        while (true) {
            lockIngredients();
            makeCocktail();
            freeIngredients();
        }
    }
}

class Main {
    public static void main(String[] args) throws InterruptedException {
        Barman lucas = new Barman("Lucas", "\u001B[32m" + "🍹Menthe à l'eau 🍹" + "\u001B[0m");
        Barman louis = new Barman("Louis", "\u001B[33m" + "🍊 Jus d'orange 🍊" + "\u001B[0m");

        Ingredient menthe = new Ingredient("Menthe");
        Ingredient glace = new Ingredient("Glace pilée");
        Ingredient limonade = new Ingredient("Limonade");
        Ingredient citronVert = new Ingredient("Citron vert");
        Ingredient orange = new Ingredient("Orange");
        Ingredient grenadine = new Ingredient("Grenadine");

        lucas.setIngredients(menthe, glace, limonade, citronVert);
        louis.setIngredients(orange, grenadine, glace, menthe);

        lucas.start();
        louis.start();

        lucas.join();
        louis.join();
    }
}
